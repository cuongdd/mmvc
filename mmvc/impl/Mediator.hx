/*
Copyright (c) 2012 Massive Interactive

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*/

package mmvc.impl;

import openfl.events.Event;
import openfl.events.IEventDispatcher;
import mmvc.base.EventMap;
import mmvc.api.IEventMap;
import openfl.display.DisplayObjectContainer;
import mmvc.base.MediatorBase;
import mmvc.api.IMediatorMap;
import minject.Injector;

/**
	Abstract MVCS `IMediator` implementation
**/
class Mediator<T> extends MediatorBase<T>
{
	@inject public var injector:Injector;
	
	@inject public var contextView:DisplayObjectContainer;
	
	@inject public var mediatorMap:IMediatorMap;

	public var eventDispatcher(default, default):IEventDispatcher;

	public var eventMap(get, null):IEventMap;
	private function get_eventMap():IEventMap {
		if(eventMap == null) eventMap = new EventMap(eventDispatcher);
		return eventMap;
	}

	public function new()
	{
		super();
	}

	override public function preRemove():Void {
		if(eventMap != null) eventMap.unmapListeners();
		super.preRemove();
	}

	public function dispatch(event:Event):Bool {
		if(eventDispatcher.hasEventListener(event.type)) return eventDispatcher.dispatchEvent(event);
		return false;
	}
}
