package mmvc.base;
import openfl.events.Event;
import openfl.events.IEventDispatcher;
import mmvc.api.IEventMap;
class EventMap implements IEventMap {

	/**
	 * The <code>IEventDispatcher</code>
	 */
	public var eventDispatcher:IEventDispatcher;

	/**
	 * @private
	 */
	public var dispatcherListeningEnabled(default, null):Bool;

	/**
	 * @private
	 */
	public var listeners:Array<EventMapConfig>;

	//---------------------------------------------------------------------
	//  Constructor
	//---------------------------------------------------------------------

	/**
	 * Creates a new <code>EventMap</code> object
	 *
	 * @param eventDispatcher An <code>IEventDispatcher</code> to treat as a bus
	 */

	public function new(eventDispatcher:IEventDispatcher) {
		listeners = new Array();
		this.eventDispatcher = eventDispatcher;
	}

	/**
	 * The same as calling <code>addEventListener</code> directly on the <code>IEventDispatcher</code>,
	 * but keeps a list of listeners for easy (usually automatic) removal.
	 *
	 * @param dispatcher The <code>IEventDispatcher</code> to listen to
	 * @param type The <code>Event</code> type to listen for
	 * @param listener The <code>Event</code> handler
	 * @param eventClass Optional Event class for a stronger mapping. Defaults to <code>flash.events.Event</code>.
	 * @param useCapture
	 * @param priority
	 * @param useWeakReference
	 */

	public function mapListener(dispatcher:IEventDispatcher, type:String, listener:Dynamic -> Void, useCapture:Bool = false, priority:Int = 0, useWeakReference:Bool = true):Void {
		if (dispatcherListeningEnabled == false && dispatcher == eventDispatcher) {
			throw new ContextError('Listening to the context eventDispatcher is not enabled for this EventMap');
		}

		var config = new EventMapConfig(dispatcher, type, listener, useCapture);
		var i = listeners.length;
		if(listeners.indexOf(config) != -1) return;
		
		config.callback =  function(event:Event):Void {
			listener(event);
		};
		listeners.push(config);
		config.addListener();
	}

	/**
	 * The same as calling <code>removeEventListener</code> directly on the <code>IEventDispatcher</code>,
	 * but updates our local list of listeners.
	 *
	 * @param dispatcher The <code>IEventDispatcher</code>
	 * @param type The <code>Event</code> type
	 * @param listener The <code>Event</code> handler
	 * @param eventClass Optional Event class for a stronger mapping. Defaults to <code>flash.events.Event</code>.
	 * @param useCapture
	 */

	public function unmapListener(dispatcher:IEventDispatcher, type:String, listener:Dynamic -> Void, useCapture:Bool = false):Void {
		var config = new EventMapConfig(dispatcher, type, listener, useCapture);
		if(listeners.remove(config)) config.removeListener();
	}

	/**
	 * Removes all listeners registered through <code>mapListener</code>
	 */

	public function unmapListeners():Void {
		var config:EventMapConfig;
		var dispatcher:IEventDispatcher;
		while (listeners.length > 0) {
			config = listeners.pop();
			config.removeListener();
		}
	}
}

class EventMapConfig {
	public var dispatcher: IEventDispatcher;
	public var type:String;
	public var listener:Dynamic -> Void;
	public var useCapture:Bool;
	public var callback:Dynamic -> Void;


	public function new(dispatcher: IEventDispatcher, type: String, listener: Dynamic->Void, useCapture: Bool) {
		this.dispatcher = dispatcher;
		this.type = type;
		this.listener = listener;
		this.useCapture = useCapture;
	}


	public inline function addListener() dispatcher.addEventListener(type, listener, useCapture);

	public inline function removeListener() dispatcher.removeEventListener(type, listener, useCapture);

	@:op(A == B)
	public function equalTo(dispatcher: IEventDispatcher, type: String, listener: Dynamic->Void, useCapture: Bool): Bool {
		return this.dispatcher == dispatcher && this.type == type && this.listener == listener && this.useCapture == useCapture;
	}

}

